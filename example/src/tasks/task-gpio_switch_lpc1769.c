/*--------------------------------------------------------------------*-

    task-gpio_switch_lpc1769.c (Released 2019-04)

  --------------------------------------------------------------------

    Simple GPIO_SWITCH task for LPC1769.
	[Read Input on LPCXpresso baseboard.]

    Simple switch interface code, without/with software debounce.

-*--------------------------------------------------------------------*/


// Project header
#include "../main/main.h"


// Task header
#include "task-gpio_switch_lpc1769.h"


// ------ Public variable ------------------------------------------
uint32_t SW_switch_pressed_G = SW_NOT_PRESSED;


// ------ Private constants ----------------------------------------
#define TEST_1 (1)	/* Test original task */
#define TEST_2 (2)	/* Test MEF switch w/debounce */


#define TEST (TEST_2)


// ------ Private variable -----------------------------------------
static uint8_t switch_input = 0;


/*------------------------------------------------------------------*-

    GPIO_SWITCH_Init()

    Prepare for GPIO_SWITCH_Update() function - see below.

-*------------------------------------------------------------------*/
void GPIO_SWITCH_Init(void)
{
	// Set up "GPIO" SW as an input pin
	Chip_IOCON_PinMux(LPC_IOCON, GPIO_SWITCH_PORT, GPIO_SWITCH_PIN, GPIO_SWITCH_PIN_MODE, GPIO_SWITCH_PIN_FUNC);
	Chip_GPIO_SetPinDIRInput(LPC_GPIO, GPIO_SWITCH_PORT, GPIO_SWITCH_PIN);

	// Switch not pressed
	SW_switch_pressed_G = SW_NOT_PRESSED;
}


#if (TEST == TEST_1)	/* Test original task */
/*------------------------------------------------------------------*-

    GPIO_SWITCH_Update()

    Simple switch interface code, without software debounce.

    If GPIO_SWITCH is not pressed, switch_pressed_G => SW_NOT_PRESSED
    If GPIO_SWITCH is     pressed, switch_pressed_G => SW_PRESSED

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void GPIO_SWITCH_Update(void)
{
	// Read GPIO_SWITCH
	switch_input = Chip_GPIO_ReadPortBit(LPC_GPIO, GPIO_SWITCH_PORT, GPIO_SWITCH_PIN);
	if (switch_input == SW_PRESSED)
	{
    	// Switch pressed
		SW_switch_pressed_G = SW_PRESSED;
	}
	else
	{
		// Switch not pressed
		SW_switch_pressed_G = SW_NOT_PRESSED;
	}
}
#endif

#if (TEST == TEST_2)	/* Test original task */
/*------------------------------------------------------------------*-

    GPIO_SWITCH_Update()

    Simple switch interface code, without software debounce.

    If GPIO_SWITCH is not pressed, switch_pressed_G => SW_NOT_PRESSED
    If GPIO_SWITCH is     pressed, switch_pressed_G => SW_PRESSED

    Must schedule every 1 mili Second (soft deadline).

-*------------------------------------------------------------------*/
void GPIO_SWITCH_Update(void)
{
	// Read GPIO_SWITCH
	static uint8_t rebotes=0,valor_anterior=0,valor_actual=0;

		valor_actual = Chip_GPIO_ReadPortBit(LPC_GPIO, TEST_GPIO_SWITCH_PORT, TEST_GPIO_SWITCH_PIN);
		if(valor_actual==valor_anterior)
			{
			rebotes++;
			valor_anterior=valor_actual;
			}
		else
			{
			rebotes=0;
			valor_anterior=valor_actual;
			}
		if(rebotes==50)
			{
			rebotes=0;
			if(valor_actual) 	SW_switch_pressed_G = SW_PRESSED;
			else				SW_switch_pressed_G = SW_NOT_PRESSED;

			}


}

#endif


/*------------------------------------------------------------------*-
  ---- END OF FILE -------------------------------------------------
-*------------------------------------------------------------------*/
